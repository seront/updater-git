#!/bin/bash

# --[  CONFIGURATION  ]--

# Remote branch to be mirrored
LOOKUP_REMOTE_BRANCH=origin/develop

# Run the pull every certain seconds
WAIT_SECONDS=180

#current Directory

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "Current directory ${CURRENT_DIR}"
# --[  ROUTINE  ]--
while true
do
  echo "--[   Updating repo  ]--"
  git --git-dir="$CURRENT_DIR/.git" --work-tree=$CURRENT_DIR fetch --all
  HEADHASH=$(git --git-dir="$CURRENT_DIR/.git" rev-parse HEAD)
  UPSTREAMHASH=$(git --git-dir="$CURRENT_DIR/.git" rev-parse master@{upstream})
  echo "HEAD: ${HEADHASH}"
  echo "UPSTREAMHASH: ${UPSTREAMHASH}"
 if [ "$HEADHASH" = "$UPSTREAMHASH" ]
 then
   echo -e Current branch is up to date with origin/master.
   
 else
   echo -e Not up to date with origin. Updating.
  git --git-dir="$CURRENT_DIR/.git" --work-tree=$CURRENT_DIR reset --hard $LOOKUP_REMOTE_BRANCH
  git --git-dir="$CURRENT_DIR/.git" --work-tree=$CURRENT_DIR pull
  systemctl restart swagger.service
  echo "Swagger.service restarted"
 fi

  sleep $WAIT_SECONDS
done
